﻿using System;
using System.Threading.Tasks;

namespace CodingChallenge.Features
{
	public interface IDispatcher
	{
		/// <summary>
		///     Dispatch this action to a background worker appropriate to the platform
		/// </summary>
		/// <param name="task">The action to execute</param>
		void ToBackground(Task task);

		/// <summary>
		///     Dispatch this action to a background worker appropriate to the platform
		/// </summary>
		/// <param name="task">The action to execute</param>
		void ToBackground(Func<Task> task);

		void ToBackground(Action task);

		/// <summary>
		///     Dispatch this action to the UI thread
		/// </summary>
		/// <param name="action">The action to execute</param>
		void ToUi(Action action);

		/// <summary>
		///     Dispatch this action to the UI thread, and await it's completion
		/// </summary>
		/// <param name="action">The action to execute</param>
		Task ToUiAsync(Action action);

		/// <summary>
		///     Dispatch this task to the UI thread
		/// </summary>
		/// <param name="task">The task to execute</param>
		void ToUi(Func<Task> task);

		/// <summary>
		///     Dispatch this action to the UI thread when the thread is next available
		/// </summary>
		/// <param name="action">The action to execute</param>
		void ToUiLazy(Action action);

		/// <summary>
		///     Dispatch this action to the UI thread when the thread is next available
		/// </summary>
		/// <param name="action">The action to execute</param>
		Task ToUiLazyAsync(Action action);

		/// <summary>
		///     Dispatch this action to the UI thread when the thread is next available
		/// </summary>
		/// <param name="action">The action to execute</param>
		void ToUiLazy(Func<Task> action);

		/// <summary>
		///     Dispatch this action to the UI thread when the thread is next available
		/// </summary>
		/// <param name="action">The action to execute</param>
		Task ToUiLazyAsync(Func<Task> action);

		/// <summary>
		///     Continue his activity in the current threading context, but using the appropriate error handling mechanism
		/// </summary>
		/// <param name="task">The action to execute</param>
		void Wrap(Func<Task> task);

		/// <summary>
		///     Continue his activity in the current threading context, but using the appropriate error handling mechanism
		/// </summary>
		/// <param name="task">The action to execute</param>
		void Wrap(Task task);
	}
}