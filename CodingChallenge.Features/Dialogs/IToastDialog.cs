﻿namespace CodingChallenge.Features.Dialogs
{
	public interface IToastDialog
	{
		void DisplayMessage(string text);	
	}
}