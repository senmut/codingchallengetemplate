﻿using System;

namespace CodingChallenge.Features.Utilities
{
	public interface IStringUtility
	{
		string ToLowerCase(string text);		
	}
}