﻿using System.Threading.Tasks;

namespace CodingChallenge.Features.Utilities
{
	public static class TaskUtility
	{
		/// <summary>
		///     Reference url:
		///     http://stackoverflow.com/questions/13127177/if-my-interface-must-return-task-what-is-the-best-way-to-have-a-no-operation-imp
		/// </summary>
		/// <returns>A no-operation task</returns>
		public static Task NoOperation()
		{
			return Task.FromResult(0);
		}

		public static Task<T> NoOperation<T>()
		{
			return Task.FromResult(default(T));
		}

		public static Task<T> NoOperation<T>(T payload)
		{
			return Task.FromResult(payload);
		}

		public static void Forget(this Task task)
		{
			task.ConfigureAwait(false);
		}
	}
}