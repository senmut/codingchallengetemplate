﻿namespace CodingChallenge.Features.Utilities
{
	public class StringUtility : IStringUtility
	{
		public string ToLowerCase(string text)
		{
			if (string.IsNullOrEmpty(text))
			{
				return text;
			}
			else
			{
				return text.ToLowerInvariant();
			}
		}
	}
}