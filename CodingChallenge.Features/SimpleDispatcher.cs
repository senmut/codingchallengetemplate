﻿using System;
using System.Threading.Tasks;
using CodingChallenge.Features.Utilities;

namespace CodingChallenge.Features
{
	public sealed class SimpleDispatcher : IDispatcher
	{
		public void ToBackground(Task task)
		{
			if (!task.IsCompleted)
			{
				Task.Run(() => task);
			}
		}

		public void ToBackground(Func<Task> taskFunc)
		{
			Task.Run(taskFunc);
		}

		public void ToBackground(Action task)
		{
			Task.Run(task);
		}

		public void ToUi(Action action)
		{
			action();
		}

		public Task ToUiAsync(Action action)
		{
			action();
			return Task.FromResult(0);
		}

		public void ToUi(Func<Task> taskFunc)
		{
			var task = taskFunc();

			if (!task.IsCompleted)
			{
				task.Wait();
			}
		}

		public void ToUiLazy(Action action)
		{
			action();
		}

		public Task ToUiLazyAsync(Action action)
		{
			action();

			return TaskUtility.NoOperation();
		}

		public void ToUiLazy(Func<Task> action)
		{
			action();
		}

		public Task ToUiLazyAsync(Func<Task> action)
		{
			action();

			return Task.Delay(0);
		}

		public void Wrap(Func<Task> task)
		{
			Wrap(task());
		}

		public void Wrap(Task task)
		{
			if (!task.IsCompleted)
			{
				task.Wait();
			}
		}
	}
}