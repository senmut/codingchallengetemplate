using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace CodingChallenge.UITests
{
	[TestFixture(Platform.Android)]
	//[TestFixture(Platform.iOS)]
	public class Tests
	{
		IApp app;
		Platform platform;

		public Tests(Platform platform)
		{
			this.platform = platform;
		}

		[SetUp]
		public void BeforeEachTest()
		{
			app = AppInitializer.StartApp(platform);
		}

		[Test]
		public void CanClickOnButton()
		{
			// Given I am on the hello screen
			app.WaitForElement(x => x.Id("display_toast"));

			// When I click on the Display toast button
			app.Tap(x => x.Id("display_toast"));

			// Then toast message is displayed for a short time
			app.WaitForElement(x => x.Id("message"));
			app.WaitForNoElement(x => x.Id("message"));
		}
	}
}

