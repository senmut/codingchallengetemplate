﻿using Android.Content;
using Android.Widget;
using CodingChallenge.Features.Dialogs;

namespace CodingChallenge.Droid.ClientSpecific
{
	public class ToastDialog : IToastDialog
	{
		private readonly Context _context;

		public ToastDialog(Context context)
		{
			_context = context;
		}

		public void DisplayMessage(string text)
		{
			Toast.MakeText(_context, text, ToastLength.Short).Show();
		}
	}
}