﻿using Android.App;
using CodingChallenge.Features.Dialogs;
using CodingChallenge.ViewModels.Models;
using MvvmCross.Platform;

namespace CodingChallenge.Droid.ClientSpecific
{
	public class AndroidDependenciesRegister : DependenciesRegister
	{
		public override void RegisterCommon()
		{
			Mvx.RegisterSingleton(Application.Context);

			Mvx.LazyConstructAndRegisterSingleton<IToastDialog, ToastDialog>();

			base.RegisterCommon();
		}
	}
}

