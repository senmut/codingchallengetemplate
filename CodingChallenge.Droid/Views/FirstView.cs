using Android.App;
using Android.OS;
using CodingChallenge.ViewModels.ViewModels;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Droid.Views;

namespace CodingChallenge.Droid.Views
{
    [Activity(Label = "View for FirstViewModel")]
	public partial class FirstView : MvxActivity<FirstViewModel>
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.FirstView);

			var set = this.CreateBindingSet<FirstView, FirstViewModel>();
			set.Bind(this.DisplayToastButton).For("Click").To(vm => vm.DisplayAsToast);
			set.Apply();
        }
    }
}