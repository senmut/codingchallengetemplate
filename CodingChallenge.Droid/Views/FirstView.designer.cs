﻿using Android.Widget;

namespace CodingChallenge.Droid.Views
{
	public partial class FirstView
	{
		private Button _displayToastButton;

		protected Button DisplayToastButton
		{
			get { return _displayToastButton ?? (_displayToastButton = FindViewById<Button>(Resource.Id.display_toast)); }
		}
	}
}