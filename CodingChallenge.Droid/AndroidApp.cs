﻿using CodingChallenge.Droid.ClientSpecific;
using CodingChallenge.ViewModels;
using CodingChallenge.ViewModels.Models;

namespace CodingChallenge.Droid
{
	internal class AndroidApp : App
	{
		protected override IDependenciesRegister CreateDepenedenciesRegister()
		{
			return new AndroidDependenciesRegister();
		}
	}
}