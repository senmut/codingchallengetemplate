using System.Collections.Generic;
using System.Reflection;
using Android.Content;
using CodingChallenge.ViewModels;
using MvvmCross.Core.ViewModels;
using MvvmCross.Droid.Platform;
using MvvmCross.Platform.Platform;

namespace CodingChallenge.Droid
{
    public class Setup : MvxAndroidSetup
    {
        public Setup(Context applicationContext) : base(applicationContext)
        {
        }

        protected override IMvxApplication CreateApp()
        {
			return new AndroidApp();
        }

        protected override IMvxTrace CreateDebugTrace()
        {
            return new DebugTrace();
        }

		protected override IEnumerable<Assembly> GetViewModelAssemblies()
		{
			return new[] { typeof(App).Assembly };
		}
    }
}