﻿using MvvmCross.Test.Core;
using MvvmCross.Core.Views;
using MvvmCross.Platform.Core;
using MvvmCross.Core.Platform;

namespace CodingChallenge.IntegrationTests
{
	public class MvxTest : MvxIoCSupportingTest
	{
		protected MockDispatcher MockDispatcher { get; private set; }

		protected override void AdditionalSetup() 
		{
			MockDispatcher = new MockDispatcher();
			Ioc.RegisterSingleton<IMvxViewDispatcher>(MockDispatcher);
			Ioc.RegisterSingleton<IMvxMainThreadDispatcher>(MockDispatcher);

			Ioc.RegisterSingleton<IMvxStringToTypeParser>(new MvxStringToTypeParser());
		}
	}
}