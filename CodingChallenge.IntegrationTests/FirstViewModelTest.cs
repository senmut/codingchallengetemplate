﻿using System;
using NUnit.Framework;
using CodingChallenge.ViewModels.ViewModels;
using Moq;
using CodingChallenge.Features.Utilities;
using CodingChallenge.Features.Dialogs;

namespace CodingChallenge.IntegrationTests
{
	[TestFixture]
	public class FirstViewModelTest : MvxTest
	{
		[Test]
		public void CanCreateViewModel()
		{
			ClearAll();

			var mockStringUtility = new Mock<IStringUtility>();
			var mockToastDialog = new Mock<IToastDialog>();
			var viewModel = new FirstViewModel(mockStringUtility.Object, mockToastDialog.Object);

			Assert.IsNotNull(viewModel);
		}
	}
}