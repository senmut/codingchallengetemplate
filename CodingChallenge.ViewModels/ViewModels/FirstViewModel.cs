using CodingChallenge.Features.Dialogs;
using CodingChallenge.Features.Utilities;
using MvvmCross.Core.ViewModels;

namespace CodingChallenge.ViewModels.ViewModels
{
    public class FirstViewModel : MvxViewModel
    {
		private string _hello = "Hello MvvmCross";
        
		private readonly IStringUtility _stringUtility;
		private readonly IToastDialog _toastDialog;

		public string Hello
        { 
            get 
			{
				return _hello; 
			}

            set 
			{
				SetProperty(ref _hello, value);
			}
        }

		public IMvxCommand DisplayAsToast
		{
			get
			{ 
				return new MvxCommand(DisplayHelloToast);
			}
		}

		public FirstViewModel(IStringUtility stringUtility, IToastDialog toastDialog) : base()
		{
			_stringUtility = stringUtility;
			_toastDialog = toastDialog;
		}

		public void DisplayHelloToast()
		{
			var text = GetLoverCaseVersion();
			_toastDialog.DisplayMessage(text);
		}

		public string GetLoverCaseVersion()
		{
			return _stringUtility.ToLowerCase(Hello);
		}
    }
}