using CodingChallenge.ViewModels.Models;
using MvvmCross.Core.ViewModels;

namespace CodingChallenge.ViewModels
{
    public abstract class App : MvxApplication
    {
		protected abstract IDependenciesRegister CreateDepenedenciesRegister();

        public override void Initialize()
        {
			var register = CreateDepenedenciesRegister();
			register.RegisterCommon();

            RegisterAppStart<ViewModels.FirstViewModel>();
        }
    }
}