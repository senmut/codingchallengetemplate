﻿using CodingChallenge.Features;
using CodingChallenge.Features.Utilities;
using MvvmCross.Platform;

namespace CodingChallenge.ViewModels.Models
{
	public abstract class DependenciesRegister : IDependenciesRegister
	{
		public virtual void RegisterCommon()
		{
			Mvx.LazyConstructAndRegisterSingleton<IDispatcher, SimpleDispatcher>();
			Mvx.LazyConstructAndRegisterSingleton<IStringUtility, StringUtility>();
		}
	}
}