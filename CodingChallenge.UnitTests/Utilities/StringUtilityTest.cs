﻿using System;
using NUnit.Framework;
using System.Threading.Tasks;
using CodingChallenge.Features.Utilities;

namespace CodingChallenge.UnitTests.Utilities
{
	[TestFixture]
	public class StringUtilityTest : IocTestBase
	{
		[TestCase("hello world", "hello world")]
		[TestCase("Do it now", "do it now")]
		[TestCase("HELLO WORLD", "hello world")]
		[TestCase("", "")]
		[TestCase(null, null)]
		public void InitialiseAsyncStartsANewSession(string text, string expected)
		{
			// Arrange
			var _stringUtility = new StringUtility();

			// Act
			var result = _stringUtility.ToLowerCase(text);

			// Assert
			Assert.AreEqual(expected, result);
		}
	}
}