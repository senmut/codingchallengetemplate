﻿using System;
using CodingChallenge.Features;
using CodingChallenge.UnitTests.Helpers;
using NUnit.Framework;
using MvvmCross.Platform.IoC;
using MvvmCross.Platform.Core;
using MvvmCross.Core;
using MvvmCross.Core.Platform;

namespace CodingChallenge.UnitTests
{
	public class IocTestBase
	{
		protected IMvxIoCProvider Ioc { get; private set; }

		protected IDispatcher Dispatcher { get; set; }

		public IocTestBase() 
		{
			Dispatcher = new SynchronousDispatcher();
		}

		[SetUp]
		public void Setup()
		{
			ReinitialiseMvxSingletons();
		}

		protected void ReinitialiseMvxSingletons()
		{
			// fake set up of the IoC
			MvxSingleton.ClearAllSingletons();
			Ioc = MvxSimpleIoCContainer.Initialize();
			Ioc.RegisterSingleton(Ioc);
			Ioc.RegisterSingleton<IMvxSettings>(new MvxSettings());
			SetupDispatcher();
			InitialiseSingletonCache();
		}

		protected virtual void SetupDispatcher()
		{
			Ioc.RegisterSingleton<IDispatcher>(() => Dispatcher);
		}

		private static void InitialiseSingletonCache()
		{
			MvxSingletonCache.Initialize();
		}
	}
}

