﻿using System;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.Generic;
using CodingChallenge.Features;

namespace CodingChallenge.UnitTests.Helpers
{
	public class SynchronousDispatcher : IDispatcher
	{
		private List<Task> _queuedTasks;

		private SemaphoreSlim queueLock = new SemaphoreSlim(1, 1);

		public SynchronousDispatcher()
		{
			_queuedTasks = new List<Task>();
		}

		public event Action Tick;

		public void RaiseTick()
		{
			if (Tick != null)
			{
				Tick();
			}
		}

		public void ToBackground(Action action)
		{
			action();
		}

		public void ToBackground(Task task)
		{
			task.Wait();
		}

		public void ToBackground(Func<Task> task)
		{
			task().Wait();
		}

		public void ToUi(Action action)
		{
			action();
		}

		public Task ToUiAsync(Action action)
		{
			action();

			return Task.FromResult(0);
		}

		public void ToUi(Func<Task> task)
		{
			task().Wait();
		}

		public void ToUiLazy(Action action)
		{
			action();
		}

		public Task ToUiLazyAsync(Action action)
		{
			action();
			return Task.FromResult(0);
		}

		public void ToUiLazy(Func<Task> action)
		{
			action().Wait();
		}

		public async Task ToUiLazyAsync(Func<Task> action)
		{
			await action();
		}

		public void Wrap(Func<Task> task)
		{
			task().Wait();
		}

		public void Wrap(Task task)
		{
			task.Wait();
		}
	}
}

